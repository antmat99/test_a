import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestClass {

    /**
     * Write your tests here.
     * Don't forget to put @Test before your test method.
     * */

    @Test
    public void test1() throws IOException {
        Adder a = new Adder(3,2);
        assertEquals(1, a.sub());
    }

    @Test
    public void test2() throws IOException {
        Multiplier m = new Multiplier(3,2);
        assertEquals(6, m.mul());
    }

    @Test
    public void test3() throws IOException {
        Multiplier m = new Multiplier(3,2);
        assertEquals(9, m.pow(2));
    }

}
